package com.easyBucketSoft19.NGR;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NGRApplication {

    public static void main(String[] args) {
        SpringApplication.run(NGRApplication.class, args);
    }

}
