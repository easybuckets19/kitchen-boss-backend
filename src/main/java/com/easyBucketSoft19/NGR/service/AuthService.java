package com.easyBucketSoft19.NGR.service;

import com.easyBucketSoft19.NGR.api.GoogleApi;
import com.easyBucketSoft19.NGR.data.input.LoginUserDTO;
import com.easyBucketSoft19.NGR.data.input.RegisterUserDTO;
import com.easyBucketSoft19.NGR.data.mapper.UserMapper;
import com.easyBucketSoft19.NGR.data.output.PlaceDetailsDTO;
import com.easyBucketSoft19.NGR.data.output.UserDTO;
import com.easyBucketSoft19.NGR.entity.user.Role;
import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.repository.user.RoleRepository;
import com.easyBucketSoft19.NGR.repository.user.UserRepository;
import com.easyBucketSoft19.NGR.security.impl.UserDetailsImpl;
import com.easyBucketSoft19.NGR.security.jwt.JwtTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class AuthService {
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder encoder;
    private final GoogleApi googleApi;
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final JwtTokenService jwtTokenService;

    @Autowired
    public AuthService(
        AuthenticationManager authenticationManager,
        PasswordEncoder encoder, GoogleApi googleApi, UserMapper userMapper, UserRepository userRepository,
        RoleRepository roleRepository,
        JwtTokenService jwtTokenService
    ) {
        this.authenticationManager = authenticationManager;
        this.encoder = encoder;
        this.googleApi = googleApi;
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.jwtTokenService = jwtTokenService;
    }


    public UserDTO login(LoginUserDTO loginUserDTO) {
        try {
            // Generate a token for the AuthenticationManager.
            UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    loginUserDTO.getUsername(), loginUserDTO.getPassword());
            // Attempting to authenticate using the token.
            Authentication authentication = authenticationManager.authenticate(token);
            // Adding the current authentication object to the context.
            SecurityContextHolder.getContext().setAuthentication(authentication);
            // Extracting our customized UserDetails object for the response.
            UserDetailsImpl details = (UserDetailsImpl) authentication.getPrincipal();
            // Generating a JWT token for the authenticated user.
            String generatedJwt = jwtTokenService.generateJWT(details);
            // Mapping the User to the associated UserDTO.
            return userMapper.toDTO(details, generatedJwt);
        } catch (AuthenticationException ex) {
            log.warn("Failed to set Authentication & clearing the context. [message: {}]", ex.getMessage());
            SecurityContextHolder.clearContext();
        }
        return null;
    }

    public UserDTO register(RegisterUserDTO registerDTO) {
        try {
            // Fetching the place data from Google Maps API.
            PlaceDetailsDTO placeDetailsDTO = googleApi.placeDetails(registerDTO.getPlaceId());
            // Checking the availability of the [given username / email] combination.
            if (!userRepository.existsByUsername(registerDTO.getUsername())
                && !userRepository.existsByEmail(registerDTO.getEmail())) {
                // Quick lookup the get a persisted entity from the DB.
                Role existingRole = roleRepository.findByRole(registerDTO.getRole()).orElse(null);
                // Mapping the DTO to a User entity with some default values. [saved role, status]
                User userData = userMapper.toEntity(registerDTO, placeDetailsDTO, existingRole);
                // Encoding the password with the custom PasswordEncoder.
                userData.setPassword(encoder.encode(userData.getPassword()));
                // Triggering a save method using the implemented repository.
                User savedUser = userRepository.save(userData);
                // Mapping the User entity to our custom DTO.
                return userMapper.toDTO(savedUser);
            }
        } catch (IOException exception) {
            log.warn("Google API JSON processing caused IOException. [message: {}]", exception.getMessage());
        }
        return null;
    }


}
