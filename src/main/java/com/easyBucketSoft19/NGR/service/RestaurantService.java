package com.easyBucketSoft19.NGR.service;

import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.data.input.CreateFoodDTO;
import com.easyBucketSoft19.NGR.data.input.CreateRestaurantDTO;
import com.easyBucketSoft19.NGR.data.input.CreateOrderDTO;
import com.easyBucketSoft19.NGR.data.output.FoodDTO;
import com.easyBucketSoft19.NGR.data.output.OrderDTO;
import com.easyBucketSoft19.NGR.data.output.RestaurantDTO;
import com.easyBucketSoft19.NGR.entity.food.Food;
import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.entity.order.Order;
import com.easyBucketSoft19.NGR.repository.food.FoodRepository;
import com.easyBucketSoft19.NGR.repository.order.OrderRepository;
import com.easyBucketSoft19.NGR.repository.restaurant.RestaurantRepository;
import com.easyBucketSoft19.NGR.security.SecurityUtils;
import com.easyBucketSoft19.NGR.utils.constant.OrderStatus;
import com.easyBucketSoft19.NGR.utils.event.OrderCreatedEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.extern.slf4j.Slf4j;

import static com.easyBucketSoft19.NGR.utils.constant.OrderStatus.*;

@Slf4j @Service
public class RestaurantService {
    final RestaurantRepository restaurantRepository;
    final OrderRepository orderRepository;
    final FoodRepository foodRepository;
    final ApplicationEventPublisher publisher;
    final MapstructMapper mapper;

    @Autowired
    public RestaurantService(RestaurantRepository restaurantRepository, OrderRepository orderRepository, FoodRepository foodRepository, ApplicationEventPublisher publisher, MapstructMapper mapper) {
        this.restaurantRepository = restaurantRepository;
        this.orderRepository = orderRepository;
        this.foodRepository = foodRepository;
        this.publisher = publisher;
        this.mapper = mapper;
    }

    public List<RestaurantDTO> getRestaurantsByOwner() {
        UserDetails userDetails = SecurityUtils.getAuthenticatedUser();
        List<Restaurant> restaurants = this.restaurantRepository.findByOwnerUsername(userDetails.getUsername());
        Stream<Restaurant> restaurantStream = restaurants.stream();
        return restaurantStream.map(mapper::map).collect(Collectors.toList());
    }

    public List<RestaurantDTO> getRestaurants() {
        List<Restaurant>    restaurants      = this.restaurantRepository.findAll();
        Stream<Restaurant>  restaurantStream = restaurants.stream();
        return restaurantStream.map(mapper::map).collect(Collectors.toList());
    }

    public RestaurantDTO getRestaurant(Integer restaurantId) {
        Optional<Restaurant> restaurant = restaurantRepository.findById(restaurantId);
        return restaurant.map(mapper::map).orElse(null);
    }

    public List<OrderDTO> getOrders(Integer restaurantId) {
        Set<OrderStatus> filters      = Set.of(STATUS_REJECTED, STATUS_ACCEPTED, STATUS_PENDING);
        List<Order>      orders       = orderRepository.findByRestaurant(restaurantId, filters);
        Stream<Order>    orderStream  = orders.stream();
        return orderStream.map(mapper::map).collect(Collectors.toList());
    }

    public List<FoodDTO> getFoods(Integer restaurantId) {
        List<Food>       foods        = foodRepository.findByRestaurant(restaurantId);
        Stream<Food>     foodStream   = foods.stream();
        return foodStream.map(mapper::map).collect(Collectors.toList());
    }


    @Transactional
    public RestaurantDTO createRestaurant(CreateRestaurantDTO createRestaurantDTO) {
        try {
            Restaurant restaurant = mapper.map(createRestaurantDTO);
            return mapper.map(restaurantRepository.save(restaurant));
        } catch (DataIntegrityViolationException ex) {
            log.warn("Restaurant with the given [name: {}] is already registered .", createRestaurantDTO.getName());
        }
        return null;
    }

    @Transactional
    public OrderDTO createOrder(CreateOrderDTO createOrderDTO) {
        Order newOrder   = mapper.map(createOrderDTO);
        createOrderDTO.getOrderLine().forEach(it -> newOrder.addLineItem(mapper.map(it)));
        Order savedOrder = orderRepository.save(newOrder);
        publisher.publishEvent(new OrderCreatedEvent(this, savedOrder));
        return mapper.map(savedOrder);
    }

    @Transactional
    public FoodDTO createFood(CreateFoodDTO createFoodDTO) {
        Food newFood    = mapper.map(createFoodDTO);
        Food savedFood  = foodRepository.save(newFood);
        return mapper.map(savedFood);
    }

}
