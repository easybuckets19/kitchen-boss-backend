package com.easyBucketSoft19.NGR.service;

import com.easyBucketSoft19.NGR.api.GoogleApi;
import com.easyBucketSoft19.NGR.data.input.UpdateUserDTO;
import com.easyBucketSoft19.NGR.data.mapper.UserMapper;
import com.easyBucketSoft19.NGR.data.output.UserDTO;
import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.repository.restaurant.RestaurantRepository;
import com.easyBucketSoft19.NGR.repository.user.UserRepository;
import com.easyBucketSoft19.NGR.security.SecurityUtils;
import com.easyBucketSoft19.NGR.utils.PredicateUtils;
import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import com.easyBucketSoft19.NGR.utils.constant.UserStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class UserService {
    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    public UserService(UserMapper userMapper, UserRepository userRepository, RestaurantRepository restaurantRepository, PasswordEncoder encoder, GoogleApi googleApi) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.encoder = encoder;
    }

    @Transactional
    public UserDTO updateUser(Integer userId, UpdateUserDTO updateUserDTO) {
        // Find the user in the DB by the given userId.
        Optional<User> user = userRepository.findById(userId);
        // Update the give User entity using a lambda.
        return user.map(existingUser -> {
            userMapper.updateEntity(updateUserDTO, existingUser);
            existingUser.setPassword(encoder.encode(updateUserDTO.getPassword()));
            User updatedUser = userRepository.save(existingUser);
            return userMapper.toDTO(updatedUser);
        }).orElse(null);
    }

    @Transactional
    public UserDTO updateUserStatus(Integer userId , UserStatus userStatus) {
        // Find the user in the DB by the given userId.
        Optional<User> user = userRepository.findById(userId);
        // Update the give User entity using a lambda.
        return user.map(existingUser -> {
            if (existingUser.getStatus() != userStatus) {
                existingUser.setStatus(userStatus);
                User updatedUser = userRepository.save(existingUser);
                return userMapper.toDTO(updatedUser);
            }
            return userMapper.toDTO(existingUser);
        }).orElse(null);
    }

}
