package com.easyBucketSoft19.NGR.service;

import com.easyBucketSoft19.NGR.api.GoogleApi;
import com.easyBucketSoft19.NGR.data.output.PlaceDetailsDTO;
import com.easyBucketSoft19.NGR.data.output.PlaceDistanceDTO;
import com.easyBucketSoft19.NGR.data.output.PlaceLookupDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Slf4j @Service
public class PlaceService {
    private final GoogleApi googleApi;

    @Autowired
    public PlaceService(GoogleApi googleApi) {
        this.googleApi = googleApi;
    }

    public List<PlaceLookupDTO> getPlaces(String placeInput) {
        try {
            return googleApi.placeAutoComplete(placeInput);
        } catch (IOException exception) {
            log.warn("Google API JSON processing failed. [message: {}]", exception.getMessage());
        }
        return null;
    }

    public PlaceDetailsDTO getPlaceDetails(String placeId) {
        try {
            return googleApi.placeDetails(placeId);
        } catch (IOException exception) {
            log.warn("Google API JSON processing failed. [message: {}]", exception.getMessage());
        }
        return null;

    }

    public PlaceDistanceDTO getPlaceDistance(String originId, String destId) {
        try {
            return googleApi.placeDistance(originId, destId);
        } catch (IOException exception) {
            log.warn("Google API JSON processing failed. [message: {}]", exception.getMessage());
        }
        return null;
    }
}
