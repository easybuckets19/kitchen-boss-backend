package com.easyBucketSoft19.NGR.service;

import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.data.output.AllergenDTO;
import com.easyBucketSoft19.NGR.data.output.CategoryDTO;
import com.easyBucketSoft19.NGR.entity.food.Allergen;
import com.easyBucketSoft19.NGR.entity.food.Category;
import com.easyBucketSoft19.NGR.repository.food.AllergenRepository;
import com.easyBucketSoft19.NGR.repository.food.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FoodService {
    private final AllergenRepository allergenRepository;
    private final CategoryRepository categoryRepository;
    private final MapstructMapper mapper;

    @Autowired
    public FoodService(AllergenRepository allergenRepository, CategoryRepository categoryRepository, MapstructMapper mapper) {
        this.allergenRepository = allergenRepository;
        this.categoryRepository = categoryRepository;
        this.mapper = mapper;
    }

    public List<String> getAllergens() {
        List<Allergen>   allergens      = allergenRepository.findAll();
        Stream<Allergen> allergenStream = allergens.stream();
        return allergenStream.map(Allergen::getAllergen).collect(Collectors.toList());
    }

    public List<String> getCategories() {
        List<Category>   categories     = categoryRepository.findAll();
        Stream<Category> categoryStream = categories.stream();
        return categoryStream.map(Category::getCategory).collect(Collectors.toList());
    }
}
