package com.easyBucketSoft19.NGR.service;

import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.utils.event.OrderAssignedEvent;
import com.easyBucketSoft19.NGR.utils.event.OrderCreatedEvent;
import com.easyBucketSoft19.NGR.utils.event.OrderStatusChangedEvent;
import com.easyBucketSoft19.NGR.utils.subscriber.OrderSubscriber;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

@Service @Slf4j
public class OrderSubscriptionService {
    private final Set<OrderSubscriber> subscribers;
    private final MapstructMapper mapper;

    @Autowired
    public OrderSubscriptionService(MapstructMapper mapper) {
        this.mapper = mapper;
        this.subscribers = new HashSet<>();
    }

    public OrderSubscriber subscribe (OrderSubscriber sub) {
        subscribers.add(sub);
        return sub;
    }

    @EventListener
    public void notifyCreated(OrderCreatedEvent orderCreatedEvent) {
        var subject = orderCreatedEvent.getOrder().getRestaurant().getOwner();
        subscribers.forEach(subscriber -> {
            if (subject.getUsername().equals(subscriber.getPrincipal().getUsername())){
                try {
                    SseEmitter.SseEventBuilder createdEvent = SseEmitter.event()
                        .data(mapper.map(orderCreatedEvent.getOrder()))
                        .name("new_order");
                    subscriber.send(createdEvent);
                    subscriber.onError(err -> {
                        subscriber.completeWithError(err);
                        subscribers.remove(subscriber);
                    });
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }

    @EventListener
    public void notifyUpdated(OrderStatusChangedEvent orderStatusChangedEvent) {
        var subject = orderStatusChangedEvent.getOrder().getRestaurant().getOwner();
        subscribers.forEach(subscriber -> {
            if (subject.getUsername().equals(subscriber.getPrincipal().getUsername())){
                try {
                    SseEmitter.SseEventBuilder createdEvent = SseEmitter.event()
                        .data(mapper.map(orderStatusChangedEvent.getOrder()))
                        .name("new_status");
                    subscriber.send(createdEvent);
                    subscriber.onError(err -> {
                        subscriber.completeWithError(err);
                        subscribers.remove(subscriber);
                    });
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }


    @EventListener
    public void notifyAssigned(OrderAssignedEvent orderAssignedEvent) {
        var subject = orderAssignedEvent.getOrder().getDelivery();
        subscribers.forEach(subscriber -> {
            if (subject.getUsername().equals(subscriber.getPrincipal().getUsername())){
                try {
                    SseEmitter.SseEventBuilder createdEvent = SseEmitter.event()
                            .data(mapper.map(orderAssignedEvent.getOrder()))
                            .name("new_job");
                    subscriber.send(createdEvent);
                    subscriber.onError(err -> {
                        subscriber.completeWithError(err);
                        subscribers.remove(subscriber);
                    });
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        });
    }
}
