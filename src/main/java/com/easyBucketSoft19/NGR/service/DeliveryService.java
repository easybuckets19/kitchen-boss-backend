package com.easyBucketSoft19.NGR.service;

import com.easyBucketSoft19.NGR.api.GoogleApi;
import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.data.input.UpdateOrderDTO;
import com.easyBucketSoft19.NGR.data.output.OrderDTO;
import com.easyBucketSoft19.NGR.data.output.UserDTO;
import com.easyBucketSoft19.NGR.entity.order.Order;
import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.repository.order.OrderRepository;
import com.easyBucketSoft19.NGR.repository.restaurant.RestaurantRepository;
import com.easyBucketSoft19.NGR.repository.user.UserRepository;
import com.easyBucketSoft19.NGR.security.SecurityUtils;
import com.easyBucketSoft19.NGR.utils.PredicateUtils;
import com.easyBucketSoft19.NGR.utils.constant.OrderStatus;
import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import com.easyBucketSoft19.NGR.utils.event.OrderAssignedEvent;
import com.easyBucketSoft19.NGR.utils.event.OrderStatusChangedEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.easyBucketSoft19.NGR.utils.constant.OrderStatus.*;

@Service
public class DeliveryService {
    private final MapstructMapper mapper;
    private final UserRepository userRepository;
    private final OrderRepository orderRepository;
    private final RestaurantRepository restaurantRepository;
    private final ApplicationEventPublisher publisher;
    private final GoogleApi googleApi;

    public DeliveryService(MapstructMapper mapper, UserRepository userRepository, OrderRepository orderRepository, RestaurantRepository restaurantRepository, ApplicationEventPublisher publisher, GoogleApi googleApi) {
        this.mapper = mapper;
        this.userRepository = userRepository;
        this.orderRepository = orderRepository;
        this.restaurantRepository = restaurantRepository;
        this.publisher = publisher;
        this.googleApi = googleApi;
    }

    public List<OrderDTO> getJobs() {
        Set<OrderStatus> filters      = Set.of(STATUS_DONE, STATUS_ACCEPTED, STATUS_PENDING);
        List<Order>      orders       = orderRepository.findByDelivery(SecurityUtils.getAuthenticatedUser().getId(), filters);
        Stream<Order>    orderStream  = orders.stream();
        return orderStream.map(mapper::map).collect(Collectors.toList());
    }

    public List<UserDTO> getDeliveries() {
        UserDetails userDetails   = SecurityUtils.getAuthenticatedUser();
        Optional<User> userOptional  = this.userRepository.findByUsername(userDetails.getUsername());
        return userOptional.map( owner -> {
            Optional<Restaurant> restaurant = restaurantRepository.findByOwner(owner);
            if (restaurant.isPresent()) {
                Stream<User> userStream                  = userRepository.findAll().stream();
                Predicate<User> deliveryFilter           = PredicateUtils.rolePredicate(UserRole.ROLE_DELIVERY);
                BiPredicate<User, Restaurant> distFilter = PredicateUtils.distancePredicate(4000L, googleApi);
                return userStream
                    .filter(deliveryFilter)
                    .filter(matched -> distFilter.test(matched, restaurant.get()))
                    .map(mapper::map).collect(Collectors.toList());
            } else {
                throw new EntityNotFoundException("");
            }
        }).orElseThrow(
                () -> new EntityNotFoundException( String.format("User was not found with given {username=%s}.", userDetails.getUsername()))
        );
    }

    public OrderDTO assignOrder(UpdateOrderDTO updateOrderDTO) {
        Optional<Order> orderOptional = orderRepository.findById(updateOrderDTO.getOrderId());
        return orderOptional.map(order -> {
            Optional<User> userOptional = userRepository.findById(updateOrderDTO.getDeliveryId());
            if (userOptional.isPresent() && userOptional.get().getRoles().iterator().next().getRole().equals(UserRole.ROLE_DELIVERY)) {
                mapper.merge(order, userOptional.get(), OrderStatus.STATUS_PENDING);
                Order saved = orderRepository.save(order);
                publisher.publishEvent(new OrderAssignedEvent(this, order));
                return mapper.map(saved);
            } else {
                throw new UnsupportedOperationException("");
            }
        }).orElseThrow(
            () -> new EntityNotFoundException( String.format("Order was not found with given {id=%s}.", updateOrderDTO.getOrderId()))
        );
    }


    public OrderDTO changeOrderStatus(UpdateOrderDTO updateOrderDTO) {
        Optional<Order> orderOptional = orderRepository.findById(updateOrderDTO.getOrderId());
        return orderOptional.map(order -> {
            if (order.getDelivery() != null) {
                mapper.merge(order , updateOrderDTO.getOrderStatus());
            } else {
                order.setOrderStatus(updateOrderDTO.getOrderStatus());
            }
            Order saved = orderRepository.save(order);
            publisher.publishEvent(new OrderStatusChangedEvent(this, order));
            return mapper.map(saved);
        }).orElseThrow(
                () -> new EntityNotFoundException( String.format("Order was not found with given {id=%s}.", updateOrderDTO.getOrderId()))
        );
    }

}
