package com.easyBucketSoft19.NGR.data.mapper;

import com.easyBucketSoft19.NGR.data.input.RegisterUserDTO;
import com.easyBucketSoft19.NGR.data.input.UpdateUserDTO;
import com.easyBucketSoft19.NGR.data.output.PlaceDetailsDTO;
import com.easyBucketSoft19.NGR.data.output.UserDTO;
import com.easyBucketSoft19.NGR.entity.user.Role;
import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.security.impl.UserDetailsImpl;
import com.easyBucketSoft19.NGR.utils.constant.UserStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.Set;

@Mapper(
    componentModel = "spring",
    imports = { UserStatus.class, Set.class }
)
public interface UserMapper {
    @Mapping(target = "roles",          expression = "java( Set.of(roleEntity) )")
    @Mapping(target = "status",         expression = "java( UserStatus.STATUS_ACCEPTED )")
    @Mapping(target = "address",        source     = "addressDTO")
    @Mapping(target = "address.radius",  source    = "registerUserDTO.radius" )
    User toEntity(RegisterUserDTO registerUserDTO, PlaceDetailsDTO addressDTO, Role roleEntity);

    @Mapping(target = "role",           expression = "java( details.getRoles().iterator().next().getRole().name() )")
    @Mapping(target = "lastName",       ignore = true)
    @Mapping(target = "firstName",      ignore = true)
    @Mapping(target = "email",          ignore = true)
    UserDTO toDTO(UserDetailsImpl details, String access_token);

    @Mapping(target = "role",           expression = "java( user.getRoles().iterator().next().getRole().name() )")
    @Mapping(target = "access_token",   ignore = true)
    UserDTO toDTO(User user);

    // TODO: Fix mapper
    void updateEntity(UpdateUserDTO updateUserDTO, @MappingTarget User user);

}
