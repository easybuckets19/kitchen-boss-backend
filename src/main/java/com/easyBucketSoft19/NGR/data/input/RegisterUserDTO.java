package com.easyBucketSoft19.NGR.data.input;

import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterUserDTO {
    @NotBlank @Length(min = 8, max = 16)    private String      username;
    @NotBlank @Length(min = 8, max = 16)    private String      password;
    @NotBlank @Length(max = 64)             private String      firstName;
    @NotBlank @Length(max = 64)             private String      lastName;
    @NotBlank @Email                        private String      email;
    @NotBlank                               private String      placeId;
    @NotNull                                private UserRole    role;
                                            private Double      radius;
}
