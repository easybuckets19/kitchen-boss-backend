package com.easyBucketSoft19.NGR.data.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginUserDTO {
    @NotBlank @Length(min = 8, max = 16)    private String  username;
    @NotBlank @Length(min = 8, max = 16)    private String  password;
}
