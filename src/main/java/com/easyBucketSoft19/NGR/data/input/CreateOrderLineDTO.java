package com.easyBucketSoft19.NGR.data.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data @Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateOrderLineDTO {
    @NotNull @Min(1)    private Integer foodId;
    @NotNull @Min(1)    private Integer qty;
}

