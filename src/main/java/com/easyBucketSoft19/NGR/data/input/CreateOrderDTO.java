package com.easyBucketSoft19.NGR.data.input;

import com.easyBucketSoft19.NGR.utils.constant.OrderType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data @Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateOrderDTO {
    @NotNull private List<CreateOrderLineDTO>   orderLine;
    @NotNull private OrderType                  type;
             private Integer                    restaurantId;
             private UserDetails                user;
}
