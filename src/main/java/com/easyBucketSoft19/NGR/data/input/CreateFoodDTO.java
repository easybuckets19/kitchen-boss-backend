package com.easyBucketSoft19.NGR.data.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data @Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateFoodDTO {
    @NotBlank   private String      name;
    @NotBlank   private String      description;
    @NotBlank   private String      category;
    @NotEmpty   private String[]    allergenes;
    @NotNull    private BigDecimal  price;
                private Integer     restaurantId;
}

