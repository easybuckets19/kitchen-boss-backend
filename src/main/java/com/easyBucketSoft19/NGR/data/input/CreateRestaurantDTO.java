package com.easyBucketSoft19.NGR.data.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data @Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateRestaurantDTO {
    @NotBlank @Length(max = 128)    private String      name;
    @Length(max = 128)              private String      description;
    @NotNull @Min(0) @Max(24)       private Short       opensAt;
    @NotNull @Min(0) @Max(24)       private Short       closesAt;
    @NotBlank                       private String      placeId;
    @NotNull                        private String[]    styles;
                                    private UserDetails user;
}
