package com.easyBucketSoft19.NGR.data.input;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateUserDTO {
    @NotBlank @Length(min = 8, max = 16)    private String  username;
    @NotBlank @Length(min = 8, max = 16)    private String  password;
    @NotBlank @Length(max = 64)             private String  firstName;
    @NotBlank @Length(max = 64)             private String  lastName;
    @NotBlank                               private String  placeId;
    @NotBlank @Email                        private String  email;
}
