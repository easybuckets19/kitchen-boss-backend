package com.easyBucketSoft19.NGR.data.input;

import com.easyBucketSoft19.NGR.utils.constant.OrderStatus;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data @Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateOrderDTO {
    @NotNull private Integer        orderId;
    @NotNull private OrderStatus    orderStatus;
             private Integer        deliveryId;
             private String         deliveryName;
}
