package com.easyBucketSoft19.NGR.data;

import com.easyBucketSoft19.NGR.data.factory.*;
import com.easyBucketSoft19.NGR.data.input.*;
import com.easyBucketSoft19.NGR.data.output.*;
import com.easyBucketSoft19.NGR.entity.food.Food;
import com.easyBucketSoft19.NGR.entity.order.Order;
import com.easyBucketSoft19.NGR.entity.order.OrderLine;
import com.easyBucketSoft19.NGR.entity.restaurant.Place;
import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.utils.constant.OrderStatus;
import com.easyBucketSoft19.NGR.utils.constant.OrderType;
import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import com.easyBucketSoft19.NGR.utils.constant.UserStatus;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Set;

@Mapper(componentModel = "spring",
    uses = {
        UserFactory.class, RoleFactory.class,
        FoodFactory.class, AllergenFactory.class , CategoryFactory.class,
        RestaurantFactory.class, StyleFactory.class, PlaceFactory.class
    },
    imports = {
        OrderType.class, UserStatus.class, Set.class
    }
)
public interface MapstructMapper {
    String USER_STATUS_EXPR     = "java( UserStatus.STATUS_ACCEPTED )";
    String ROLES_ITER_EXPR      = "java( entity.getRoles().iterator().next().getRole().name() )";
    String DELIVERY_ADDR_EXPR   = "java( entity.getOrderType().equals(com.easyBucketSoft19.NGR.utils.constant.OrderType.TYPE_DELIVERY) ? entity.getCustomer().getAddress().getAddress() : entity.getRestaurant().getAddress().getAddress() )";

    /* --------------------- MAPPING METHODS - ORDERS (CRUD + relationships)  ------------------------- */
    @Mapping(target = "orderStatus",    expression  = "java(com.easyBucketSoft19.NGR.utils.constant.OrderStatus.STATUS_PENDING)")
    @Mapping(target = "orderType",      source      = "DTO.type")
    @Mapping(target = "customer",       source      = "DTO.user"                        )
    @Mapping(target = "restaurant",     source      = "DTO.restaurantId"                )
    @Mapping(target = "orderLines",     ignore      = true                              )
    @Mapping(target = "delivery",       ignore      = true                              )
    @Mapping(target = "total",          ignore      = true                              )
    Order map(CreateOrderDTO DTO);

    @Mapping(target = "delivery",       source      = "delivery"                        )
    @Mapping(target = "orderStatus",    source      = "orderStatus"                     )
    @Mapping(target = "restaurant",     ignore      = true                              )
    @Mapping(target = "orderType",      ignore      = true                              )
    @Mapping(target = "orderLines",     ignore      = true                              )
    @Mapping(target = "customer",       ignore      = true                              )
    @Mapping(target = "total",          ignore      = true                              )
    void merge(@MappingTarget Order entity, User delivery, OrderStatus orderStatus);

    @Mapping(target = "delivery",       expression  = "java( orderStatus.equals(OrderStatus.STATUS_REJECTED) ? null : entity.getDelivery() )"                    )
    @Mapping(target = "orderStatus",    source      = "orderStatus"                     )
    @Mapping(target = "restaurant",     ignore      = true                              )
    @Mapping(target = "orderType",      ignore      = true                              )
    @Mapping(target = "orderLines",     ignore      = true                              )
    @Mapping(target = "customer",       ignore      = true                              )
    @Mapping(target = "total",          ignore      = true                              )
    void merge(@MappingTarget Order entity, OrderStatus orderStatus);

    @Mapping(target = "food",           source      = "DTO.foodId"                      )
    @Mapping(target = "quantity",       source      = "DTO.qty"                         )
    @Mapping(target = "currentPrice",   ignore      = true                              )
    @Mapping(target = "order",          ignore      = true                              )
    @Mapping(target = "lineTotal",      ignore      = true                              )
    OrderLine map(CreateOrderLineDTO DTO);

    @Mapping(target = "address",        expression  = DELIVERY_ADDR_EXPR                )
    @Mapping(target = "time",           source      = "entity.createdAt"                )
    @Mapping(target = "type",           source      = "entity.orderType"                )
    @Mapping(target = "orderLines",     source      = "entity.orderLines"               )
    @Mapping(target = "orderedBy",      source      = "entity.customer.username"        )
    @Mapping(target = "deliveredBy",    source      = "entity.delivery.username"        )
    @Mapping(target = "restaurant",     source      = "entity.restaurant.name"          )
    OrderDTO map(Order entity);

    @Mapping(target = "food",           source      = "entity.food.name"                )
    @Mapping(target = "price",          source      = "entity.currentPrice"             )
    OrderLineDTO map(OrderLine entity);
    /* ------------------------------------------------------------------------------------------------ */


    /* --------------------- MAPPING METHODS - RESTAURANTS (CRUD + relationships)  -------------------- */
    @Mapping(target = "styles",        source       = "DTO.styles"                      )
    @Mapping(target = "address",       source       = "DTO.placeId"                     )
    @Mapping(target = "owner",         source       = "DTO.user"                        )
    Restaurant map(CreateRestaurantDTO DTO);

    @Mapping(target = "styles",        source       = "entity.styles"                   )
    @Mapping(target = "address",       source       = "entity.address.address"          )
    RestaurantDTO map(Restaurant entity);
    /* ------------------------------------------------------------------------------------------------ */


    /* --------------------- MAPPING METHODS - FOODS (CRUD + relationships)  -------------------------- */
    @Mapping(target = "restaurant",   source = "DTO.restaurantId"                       )
    @Mapping(target = "allergens",    source = "DTO.allergenes"                         )
    Food map(CreateFoodDTO DTO);

    @Mapping(target = "allergenes",   source = "allergens"                              )
    FoodDTO map(Food entity);

    @Mapping(target = "radius",       ignore = true                                     )
    Place map(PlaceDetailsDTO DTO);

    /* ------------------------------------------------------------------------------------------------ */

    /* --------------------- MAPPING METHODS - USERS (CRUD + relationships)  -------------------------- */
    @Mapping(target = "status",          expression = USER_STATUS_EXPR                   )
    @Mapping(target = "roles",           source     = "roles"                            )
    @Mapping(target = "address",         source     = "DTO.placeId"                      )
    @Mapping(target = "address.radius",  source     = "DTO.radius"                       )
    User map(RegisterUserDTO DTO, Set<UserRole> roles);

    @Mapping(target = "role",            expression = ROLES_ITER_EXPR                    )
    @Mapping(target = "access_token",    ignore = true                                   )
    UserDTO map(User entity);

    static UpdateOrderDTO convertDTO(String deliveryName, UpdateOrderDTO DTO) {
        return DTO.setDeliveryName(deliveryName);
    }

    static UpdateOrderDTO convertDTO(Integer deliveryId, UpdateOrderDTO DTO) {
        return DTO.setDeliveryId(deliveryId);
    }

    static CreateRestaurantDTO convertDTO(UserDetails principal, CreateRestaurantDTO DTO) {
        return DTO.setUser(principal);
    }

    static CreateOrderDTO convertDTO(Integer restaurantId, UserDetails principal, CreateOrderDTO DTO){
        return DTO.setRestaurantId(restaurantId).setUser(principal);
    }

    static CreateFoodDTO convertDTO(Integer restaurantId, CreateFoodDTO DTO) {
        return DTO.setRestaurantId(restaurantId);
    }
}
