package com.easyBucketSoft19.NGR.data.factory;

import com.easyBucketSoft19.NGR.entity.user.Role;
import com.easyBucketSoft19.NGR.repository.user.RoleRepository;
import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class RoleFactory {
    private final RoleRepository roleRepository;

    public RoleFactory(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @SuppressWarnings("unused")
    public Role resolveEntity(UserRole role, @TargetType Class<Role> type) {
        return roleRepository.findByRole(role).orElseThrow(() -> new EntityNotFoundException(
                String.format("Role was not found with given {name=%s}", role.name())
            )
        );
    }

    @SuppressWarnings("unused")
    public UserRole resolveDTO(Role role, @TargetType Class<UserRole> type) {
        return role.getRole();
    }

}
