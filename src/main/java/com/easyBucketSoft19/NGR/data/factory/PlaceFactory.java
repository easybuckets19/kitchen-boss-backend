package com.easyBucketSoft19.NGR.data.factory;

import com.easyBucketSoft19.NGR.api.GoogleApi;
import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.data.output.PlaceDetailsDTO;
import com.easyBucketSoft19.NGR.entity.restaurant.Place;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.TargetType;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component @Slf4j
public class PlaceFactory {
    private final MapstructMapper mapper;
    private final GoogleApi api;

    public PlaceFactory(MapstructMapper mapper, GoogleApi api) {
        this.mapper = mapper;
        this.api = api;
    }

    public Place resolveEntity(String placeId, @TargetType Class<Place> type) {
        try {
            PlaceDetailsDTO placeDetailsDTO = api.placeDetails(placeId);
            return mapper.map(placeDetailsDTO);
        } catch (IOException exception) {
            log.warn("Google API JSON processing caused IOException. [message: {}]", exception.getMessage());
        }
        return new Place();
    }

}
