package com.easyBucketSoft19.NGR.data.factory;

import com.easyBucketSoft19.NGR.entity.food.Food;
import com.easyBucketSoft19.NGR.repository.food.FoodRepository;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Component
public class FoodFactory {
    private final FoodRepository repository;

    public FoodFactory(FoodRepository repository) {
        this.repository = repository;
    }

    @SuppressWarnings("unused")
    public Food resolveEntity(Integer foodId, @TargetType Class<Food> type) {
        Optional<Food> foodOptional = repository.findById(foodId);
        return foodOptional.orElse(null);
        // String.format("Food was not found with given {id=%s}", foodId)
    }
}
