package com.easyBucketSoft19.NGR.data.factory;

import com.easyBucketSoft19.NGR.entity.food.Allergen;
import com.easyBucketSoft19.NGR.repository.food.AllergenRepository;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AllergenFactory {
    private final AllergenRepository repository;

    public AllergenFactory(AllergenRepository repository) {
        this.repository = repository;
    }

    public Allergen resolveEntity(String allergenName, @TargetType Class<Allergen> type) {
        Optional<Allergen> allergenOptional = repository.findByAllergen(allergenName);
        return allergenOptional.orElse(new Allergen(allergenName));
    }

    public String resolveDTO(Allergen allergen, @TargetType Class<String> type) {
        return allergen.getAllergen();
    }

}
