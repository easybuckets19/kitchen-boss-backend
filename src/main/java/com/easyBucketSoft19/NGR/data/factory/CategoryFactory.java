package com.easyBucketSoft19.NGR.data.factory;

import com.easyBucketSoft19.NGR.entity.food.Category;
import com.easyBucketSoft19.NGR.repository.food.CategoryRepository;
import org.mapstruct.TargetType;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component

public class CategoryFactory {
    private final CategoryRepository repository;

    public CategoryFactory(CategoryRepository repository) {
        this.repository = repository;
    }

    public Category resolveEntity(String categoryName, @TargetType Class<Category> type) {
        Optional<Category> categoryOptional = repository.findByCategory(categoryName);
        return categoryOptional.orElse(new Category(categoryName));
    }

    public String resolveDTO(Category category, @TargetType Class<String> type) {
        return category.getCategory();
    }

}
