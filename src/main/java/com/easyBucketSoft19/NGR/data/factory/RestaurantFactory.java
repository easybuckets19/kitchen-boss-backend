package com.easyBucketSoft19.NGR.data.factory;

import com.easyBucketSoft19.NGR.entity.food.Food;
import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.repository.restaurant.RestaurantRepository;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Component
public class RestaurantFactory {
    private final RestaurantRepository repository;

    public RestaurantFactory(RestaurantRepository repository) {
        this.repository = repository;
    }

    @SuppressWarnings("unused")
    public Restaurant resolveEntity(Integer restaurantId, @TargetType Class<Restaurant> type) {
        Optional<Restaurant> restaurantOptional = repository.findById(restaurantId);
        return restaurantOptional.orElse(null);
        // String.format("Restaurant was not found with given {id=%s}", restaurantId)
    }

}
