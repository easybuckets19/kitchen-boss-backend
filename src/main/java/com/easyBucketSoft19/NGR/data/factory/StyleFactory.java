package com.easyBucketSoft19.NGR.data.factory;

import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.entity.restaurant.Style;
import com.easyBucketSoft19.NGR.repository.restaurant.StyleRepository;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class StyleFactory {
    private final StyleRepository repository;

    public StyleFactory(StyleRepository repository) {
        this.repository = repository;
    }

    public Style resolveEntity(String styleName, @TargetType Class<Style> type) {
        Optional<Style> styleOptional = repository.findByStyle(styleName);
        return styleOptional.orElse(new Style(styleName));
    }

    public String resolveDTO(Style style, @TargetType Class<String> type) {
        return style.getStyle();
    }

}
