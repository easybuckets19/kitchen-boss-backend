package com.easyBucketSoft19.NGR.data.factory;

import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.repository.user.UserRepository;
import org.hibernate.cfg.NotYetImplementedException;
import org.mapstruct.TargetType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class UserFactory {
    private final UserRepository repository;

    public UserFactory(UserRepository repository) {
        this.repository = repository;
    }

    @SuppressWarnings("unused")
    public User resolveEntity(UserDetails userDetails, @TargetType Class<User> type) {
        return repository.findByUsername(userDetails.getUsername()).orElseThrow(() -> new EntityNotFoundException(
                String.format("User was not found with given {username=%s}", userDetails.getUsername())
            )
        );
    }

    @SuppressWarnings("unused")
    public User resolveEntity(Integer userId, @TargetType Class<User> type) {
        return repository.findById(userId).orElseThrow(() -> new EntityNotFoundException(
                String.format("User was not found with given {id=%s}", userId)
            )
        );
    }

}
