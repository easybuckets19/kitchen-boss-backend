package com.easyBucketSoft19.NGR.data.serializer;

import com.easyBucketSoft19.NGR.data.output.PlaceDetailsDTO;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class PlaceSerializer extends StdDeserializer<PlaceDetailsDTO> {

    public PlaceSerializer(){
        this(null);
    }

    public PlaceSerializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public PlaceDetailsDTO deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
        JsonNode dataNode = parser.getCodec().readTree(parser);
        return PlaceDetailsDTO.builder()
            .id(dataNode.get("place_id").asText())
            .address(dataNode.get("formatted_address").asText())
            .lat(dataNode.get("geometry").get("location").get("lat").asDouble())
            .lng(dataNode.get("geometry").get("location").get("lng").asDouble())
            .build();
    }
}

