package com.easyBucketSoft19.NGR.data.serializer;

import com.easyBucketSoft19.NGR.data.output.PlaceLookupDTO;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class PlaceLookupSerializer extends StdDeserializer<PlaceLookupDTO> {

    public PlaceLookupSerializer(){
        this(null);
    }

    public PlaceLookupSerializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public PlaceLookupDTO deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
        JsonNode dataNode = parser.getCodec().readTree(parser);
        return PlaceLookupDTO.builder()
            .id(dataNode.get("place_id").asText())
            .address(dataNode.get("description").asText())
            .build();
    }
}

