package com.easyBucketSoft19.NGR.data.serializer;

import com.easyBucketSoft19.NGR.data.output.PlaceDistanceDTO;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class PlaceDistanceSerializer extends StdDeserializer<PlaceDistanceDTO> {

    public PlaceDistanceSerializer(){
        this(null);
    }

    public PlaceDistanceSerializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public PlaceDistanceDTO deserialize(JsonParser parser, DeserializationContext ctx) throws IOException {
        JsonNode dataNode = parser.getCodec().readTree(parser);
        return PlaceDistanceDTO.builder()
            .source(dataNode.get("origin_addresses").get(0).asText())
            .dest(dataNode.get("destination_addresses").get(0).asText())
            .distance(dataNode.get("rows").get(0).get("elements").get(0).get("distance").get("value").asLong())
            //.distanceString(dataNode.get("rows").get(0).get("elements").get(0).get("distance").get("text").asText())
            .build();
    }
}

