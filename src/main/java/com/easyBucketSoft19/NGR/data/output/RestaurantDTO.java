package com.easyBucketSoft19.NGR.data.output;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestaurantDTO {
    private Integer     id;
    private String      name;
    private String      description;
    private Short       opensAt;
    private Short       closesAt;
    private String      address;
    private String[]    styles;
}
