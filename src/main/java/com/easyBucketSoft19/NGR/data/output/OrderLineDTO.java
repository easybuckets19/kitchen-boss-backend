package com.easyBucketSoft19.NGR.data.output;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderLineDTO {
    private Integer     id;
    private String      food;
    private BigDecimal  price;
    private Integer     quantity;
    private BigDecimal  lineTotal;
}
