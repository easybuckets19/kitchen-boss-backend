package com.easyBucketSoft19.NGR.data.output;

import com.easyBucketSoft19.NGR.utils.constant.OrderStatus;
import com.easyBucketSoft19.NGR.utils.constant.OrderType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderDTO {
    private Integer             id;
    private LocalDateTime       time;
    private OrderType           type;
    private String              restaurant;
    private String              address;
    private OrderStatus         orderStatus;
    private OrderLineDTO[]      orderLines;
    private String              orderedBy;
    private String              deliveredBy;
    private BigDecimal          total;
}
