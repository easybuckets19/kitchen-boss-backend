package com.easyBucketSoft19.NGR.data.output;

import com.easyBucketSoft19.NGR.data.serializer.PlaceSerializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;

@Data @Builder
@JsonDeserialize(using = PlaceSerializer.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceDetailsDTO {
    private String  id;
    private String  address;
    private Double  lat;
    private Double  lng;
}
