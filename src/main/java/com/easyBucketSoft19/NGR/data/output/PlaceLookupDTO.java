package com.easyBucketSoft19.NGR.data.output;

import com.easyBucketSoft19.NGR.data.serializer.PlaceLookupSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;

@Data @Builder
@JsonDeserialize(using = PlaceLookupSerializer.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceLookupDTO {
    private String  id;
    private String  address;
}
