package com.easyBucketSoft19.NGR.data.output;


import com.easyBucketSoft19.NGR.data.serializer.PlaceDistanceSerializer;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;

@Data @Builder
@JsonDeserialize(using = PlaceDistanceSerializer.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlaceDistanceDTO {
    private String     source;
    private String     dest;
    private Long       distance;
}
