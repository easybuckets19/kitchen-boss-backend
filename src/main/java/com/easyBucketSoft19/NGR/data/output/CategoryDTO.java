package com.easyBucketSoft19.NGR.data.output;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data @Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CategoryDTO {
    private Integer     id;
    private String      name;
}
