package com.easyBucketSoft19.NGR.controller;

import com.easyBucketSoft19.NGR.data.input.LoginUserDTO;
import com.easyBucketSoft19.NGR.data.input.RegisterUserDTO;
import com.easyBucketSoft19.NGR.data.output.UserDTO;
import com.easyBucketSoft19.NGR.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    ResponseEntity<UserDTO> loginUser(
            @Valid @RequestBody LoginUserDTO loginUserDTO) {
        UserDTO userDTO = this.authService.login(loginUserDTO);
        return (userDTO == null) ? ResponseEntity.badRequest().build() : ResponseEntity.ok(userDTO);
    }

    @PostMapping("/register")
    ResponseEntity<?> registerUser(
            @Valid @RequestBody RegisterUserDTO registerUserDTO) {
        UserDTO userDTO = this.authService.register(registerUserDTO);
        System.out.println(userDTO);
        return (userDTO == null) ? ResponseEntity.badRequest().build() : ResponseEntity.ok(userDTO);
    }

}
