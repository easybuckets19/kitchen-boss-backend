package com.easyBucketSoft19.NGR.controller;

import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.data.input.CreateFoodDTO;
import com.easyBucketSoft19.NGR.service.FoodService;
import com.easyBucketSoft19.NGR.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class FoodController {
    private final FoodService foodService;
    private final RestaurantService restaurantService;

    @Autowired
    public FoodController(FoodService foodService, RestaurantService restaurantService) {
        this.foodService = foodService;
        this.restaurantService = restaurantService;
    }

    @PreAuthorize("hasRole('OWNER')")
    @GetMapping("/food/allergenes")
    ResponseEntity<?> getAllergens() {
        return new ResponseEntity<>(this.foodService.getAllergens(), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('OWNER')")
    @GetMapping("/food/categories")
    ResponseEntity<?> getCategories() {
        return new ResponseEntity<>(this.foodService.getCategories(), HttpStatus.OK);
    }

    @GetMapping("/restaurants/{restaurantId}/food")
    ResponseEntity<?> getFoodByRestaurantId(
            @PathVariable Integer restaurantId) {
        return new ResponseEntity<>(this.restaurantService.getFoods(restaurantId), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('OWNER')")
    @PostMapping("/restaurants/{restaurantId}/food")
    ResponseEntity<?> createFood(
            @PathVariable Integer restaurantId,
            @RequestBody @Valid CreateFoodDTO createFoodDTO) {
        CreateFoodDTO DTO = MapstructMapper.convertDTO(restaurantId, createFoodDTO);
        return new ResponseEntity<>(this.restaurantService.createFood(DTO), HttpStatus.CREATED);
    }

}
