package com.easyBucketSoft19.NGR.controller;

import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.data.input.CreateRestaurantDTO;
import com.easyBucketSoft19.NGR.security.SecurityUtils;
import com.easyBucketSoft19.NGR.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/restaurants")
public class RestaurantController {
    final RestaurantService restaurantService;

    public RestaurantController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @GetMapping()
    ResponseEntity<?> getAllRestaurants() {
        var responseBody = this.restaurantService.getRestaurants();

        if (responseBody.size() == 0)
            return ResponseEntity.status(NO_CONTENT).build();
        else
            return ResponseEntity.ok(responseBody);
    }

    @GetMapping("/by-owner")
    ResponseEntity<?> getOwnerestaurants() {
        var responseBody = this.restaurantService.getRestaurantsByOwner();

        if (responseBody.size() == 0)
            return ResponseEntity.status(NO_CONTENT).build();
        else
            return ResponseEntity.ok(responseBody);
    }

    @GetMapping("/{restaurantId}")
    ResponseEntity<?> getRestaurantById(@PathVariable Integer restaurantId) {
        var responseBody = this.restaurantService.getRestaurant(restaurantId);

        if (responseBody == null)
            return ResponseEntity.status(NO_CONTENT).build();
        else
            return ResponseEntity.ok(responseBody);
    }

    @PostMapping()@PreAuthorize("hasAnyRole('OWNER')")
    ResponseEntity<?> createRestaurant(@Valid @RequestBody CreateRestaurantDTO createRestaurantDTO) {
        var responseBody = this.restaurantService.createRestaurant(
            MapstructMapper.convertDTO(SecurityUtils.getAuthenticatedUser(), createRestaurantDTO)
        );

        if (responseBody == null)
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).build();
        else
            return ResponseEntity.ok(responseBody);
    }

}
