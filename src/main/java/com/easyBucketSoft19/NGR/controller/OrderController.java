package com.easyBucketSoft19.NGR.controller;

import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.data.input.CreateOrderDTO;
import com.easyBucketSoft19.NGR.security.SecurityUtils;
import com.easyBucketSoft19.NGR.service.RestaurantService;
import com.easyBucketSoft19.NGR.utils.subscriber.OrderSubscriber;
import com.easyBucketSoft19.NGR.service.OrderSubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class OrderController {
    private final OrderSubscriptionService orderSubscriptionService;
    private final RestaurantService restaurantService;

    @Autowired
    public OrderController( OrderSubscriptionService orderSubscriptionService, RestaurantService restaurantService) {
        this.orderSubscriptionService = orderSubscriptionService;
        this.restaurantService = restaurantService;
    }

    @PreAuthorize("hasAnyRole('OWNER', 'DELIVERY')")
    @GetMapping("/sse-connect")
    OrderSubscriber connectSub() {
        UserDetails userDetails    = SecurityUtils.getAuthenticatedUser();
        OrderSubscriber subscriber = new OrderSubscriber(userDetails);
        return this.orderSubscriptionService.subscribe(subscriber);
    }

    @PreAuthorize("hasRole('OWNER')")
    @GetMapping("/restaurants/{restaurantId}/orders")
    ResponseEntity<?> createOrder(
            @PathVariable       Integer restaurantId) {
        return new ResponseEntity<>(this.restaurantService.getOrders(restaurantId), HttpStatus.OK);
    }

    @PreAuthorize("hasRole('CUSTOMER')")
    @PostMapping("/restaurants/{restaurantId}/orders")
    ResponseEntity<?> createOrder(
            @PathVariable       Integer restaurantId,
            @RequestBody @Valid CreateOrderDTO createOrderDTO) {
        UserDetails userDetails = SecurityUtils.getAuthenticatedUser();
        CreateOrderDTO DTO      = MapstructMapper.convertDTO(restaurantId, userDetails, createOrderDTO);
        return new ResponseEntity<>(this.restaurantService.createOrder(DTO), HttpStatus.CREATED);
    }


}

