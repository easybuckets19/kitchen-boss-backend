package com.easyBucketSoft19.NGR.controller;

import com.easyBucketSoft19.NGR.data.MapstructMapper;
import com.easyBucketSoft19.NGR.data.input.CreateRestaurantDTO;
import com.easyBucketSoft19.NGR.data.input.UpdateOrderDTO;
import com.easyBucketSoft19.NGR.data.output.OrderDTO;
import com.easyBucketSoft19.NGR.data.output.UserDTO;
import com.easyBucketSoft19.NGR.security.SecurityUtils;
import com.easyBucketSoft19.NGR.service.DeliveryService;
import com.easyBucketSoft19.NGR.service.FoodService;
import com.easyBucketSoft19.NGR.service.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController @RequestMapping("/delivery")
public class DeliveryController {
    private final DeliveryService deliveryService;

    @Autowired
    public DeliveryController(DeliveryService deliveryService) {
        this.deliveryService = deliveryService;
    }

    @RequestMapping("/all") @PreAuthorize("hasRole('OWNER')")
    ResponseEntity<?> getDeliveries() {
        List<UserDTO> userDTOs  = this.deliveryService.getDeliveries();
        return new ResponseEntity<>(userDTOs, HttpStatus.OK);
    }

    @RequestMapping("/current-jobs") @PreAuthorize("hasRole('DELIVERY')")
    ResponseEntity<?> getActiveJobs() {
        List<OrderDTO> orderDTOs  = this.deliveryService.getJobs();
        return new ResponseEntity<>(orderDTOs, HttpStatus.OK);
    }

    @PostMapping("/new-job/{id}") @PreAuthorize("hasRole('OWNER')")
    ResponseEntity<?> assignOrder (
            @PathVariable("id") Integer deliveryId,
            @Valid @RequestBody UpdateOrderDTO updateOrderDTO) {
        UpdateOrderDTO DTO = MapstructMapper.convertDTO(deliveryId, updateOrderDTO);
        OrderDTO orderDTO  = this.deliveryService.assignOrder(DTO);
        return new ResponseEntity<>(orderDTO, HttpStatus.CREATED);
    }
    
    @PostMapping("/job-status") @PreAuthorize("hasAnyRole('OWNER', 'DELIVERY')")
    ResponseEntity<?> assignOrder (
            @Valid @RequestBody UpdateOrderDTO updateOrderDTO) {
        UserDetails userDetails = SecurityUtils.getAuthenticatedUser();
        UpdateOrderDTO DTO      = MapstructMapper.convertDTO(userDetails.getUsername(), updateOrderDTO);
        OrderDTO orderDTO       = this.deliveryService.changeOrderStatus(DTO);
        return new ResponseEntity<>(orderDTO, HttpStatus.CREATED);
    }
}
