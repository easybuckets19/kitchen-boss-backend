package com.easyBucketSoft19.NGR.controller;

import com.easyBucketSoft19.NGR.data.input.UpdateUserDTO;
import com.easyBucketSoft19.NGR.data.output.UserDTO;
import com.easyBucketSoft19.NGR.service.UserService;
import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import com.easyBucketSoft19.NGR.utils.constant.UserStatus;
import org.hibernate.cfg.NotYetImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    @PutMapping("/users/{userId}")
    ResponseEntity<UserDTO> updateUser(
            @PathVariable Integer userId,
            @Valid @RequestBody UpdateUserDTO updateUserDto) {
        UserDTO userDTO = this.userService.updateUser(userId, updateUserDto);
        return (userDTO == null) ? ResponseEntity.badRequest().build() : ResponseEntity.ok(userDTO);
    }

    @PatchMapping("/users/{userId}") @PreAuthorize("hasRole('ADMIN')")
    ResponseEntity<?> updateUserStatus(
            @PathVariable Integer userId,
            @RequestParam("user_status") UserStatus userStatus) {
        UserDTO userDTO = this.userService.updateUserStatus(userId, userStatus);
        return (userDTO == null) ? ResponseEntity.badRequest().build() : ResponseEntity.ok(userDTO);
    }

}
