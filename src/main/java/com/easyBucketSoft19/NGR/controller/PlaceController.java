package com.easyBucketSoft19.NGR.controller;

import com.easyBucketSoft19.NGR.data.output.PlaceDetailsDTO;
import com.easyBucketSoft19.NGR.data.output.PlaceDistanceDTO;
import com.easyBucketSoft19.NGR.data.output.PlaceLookupDTO;
import com.easyBucketSoft19.NGR.service.PlaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

@RestController
@RequestMapping("/places")
public class PlaceController {
    final PlaceService placeService;

    @Autowired
    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping("/search")
    ResponseEntity<?> getPlaces(@RequestParam(name = "place_name") String placeName) {
        var responseBody = this.placeService.getPlaces(placeName);

        if (responseBody == null)
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).build();
        else
            return ResponseEntity.ok(responseBody);
    }

    @GetMapping("/details")
    ResponseEntity<?> getPlaceDetails(@RequestParam(name = "place_id") String placeId) {
        var responseBody = this.placeService.getPlaceDetails(placeId);

        if (responseBody == null)
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).build();
        else
            return ResponseEntity.ok(responseBody);
    }

    @GetMapping("/distance")
    ResponseEntity<?> getPlaceDistance(@RequestParam(name = "origin_id") String originId, @RequestParam(name = "dest_id") String destId) {
        var responseBody = this.placeService.getPlaceDistance(originId, destId);

        if (responseBody == null)
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).build();
        else
            return ResponseEntity.ok(responseBody);
    }

}
