package com.easyBucketSoft19.NGR.entity.restaurant;

import lombok.*;

import javax.persistence.*;

@Entity(name = "styles")
@Getter @Setter
public class Style {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Short id;
    /* ------------------------------------------------------------------------------------ */

    @Column(nullable = false, unique = true)
    private String style;

    public Style() {}

    public Style(String style) {
        this.style = style;
    }

}

