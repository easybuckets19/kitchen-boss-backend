package com.easyBucketSoft19.NGR.entity.restaurant;

import lombok.*;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "places")
@Getter @Setter
public class Place {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id
    private String id;
    /* ------------------------------------------------------------------------------------ */

    @Column(nullable = false)
    private String address;

    @Column(precision = 10, scale = 6)
    private Double lat;

    @Column(precision = 10, scale = 6)
    private Double lng;

    @Column(precision = 10, scale = 6)
    private Long radius;

    public Place() {}

    public Place(String id, String address, Double lat, Double lng, Long radius) {
        this.id = id;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.radius = radius;
    }

    /* --------------------------------- ENTITY LIFECYCLE ---------------------------------  */
    @PrePersist                 private void onPersist() {
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }
    @PreUpdate                  private void onUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
    @Setter(AccessLevel.NONE)   private LocalDateTime createdAt;
    @Setter(AccessLevel.NONE)   private LocalDateTime updatedAt;
    /* ------------------------------------------------------------------------------------  */

}
