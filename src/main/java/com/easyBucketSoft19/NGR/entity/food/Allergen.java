package com.easyBucketSoft19.NGR.entity.food;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter @Setter
@Table(name = "allergenes")
public class Allergen {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Short id;
    /* ------------------------------------------------------------------------------------ */

    @Column(nullable = false, unique = true)
    private String allergen;

    public Allergen() {}

    public Allergen(String allergen) {
        this.allergen = allergen;
    }

}
