package com.easyBucketSoft19.NGR.entity.food;

import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "foods")
@Getter @Setter
public class Food {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Integer id;
    /* ------------------------------------------------------------------------------------ */

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @Column(nullable = false)
    private BigDecimal price;

    /* ------------------------------- ENTITY RELATIONSHIPS -------------------------------  */
    @ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    private Restaurant restaurant;

    @ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinTable(name = "food_allergen",
            joinColumns = @JoinColumn(name = "food_id"),
            inverseJoinColumns = @JoinColumn(name = "allergen_id")
    )
    private Set<Allergen> allergens = new HashSet<>();
    /* ------------------------------------------------------------------------------------ */

    public Food() {}

    /* --------------------------------- ENTITY LIFECYCLE ---------------------------------  */
    @PrePersist                 private void onPersist() {
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }
    @PreUpdate                  private void onUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
    @Setter(AccessLevel.NONE)   private LocalDateTime createdAt;
    @Setter(AccessLevel.NONE)   private LocalDateTime updatedAt;
    /* ------------------------------------------------------------------------------------  */

}
