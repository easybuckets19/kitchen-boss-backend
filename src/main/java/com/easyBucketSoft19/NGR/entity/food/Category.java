package com.easyBucketSoft19.NGR.entity.food;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter @Setter
@Table(name = "categories")
public class Category {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Short id;
    /* ------------------------------------------------------------------------------------ */

    @Column(nullable = false, unique = true)
    private String category;

    public Category() {}

    public Category(String category) {
        this.category = category;
    }

    /* --------------------------------- ENTITY LIFECYCLE ---------------------------------  */
    @PrePersist                 private void onPersist() {
        this.createdAt = LocalDateTime.now();
    }
    @Setter(AccessLevel.NONE)   private LocalDateTime createdAt;
    /* ------------------------------------------------------------------------------------  */
}
