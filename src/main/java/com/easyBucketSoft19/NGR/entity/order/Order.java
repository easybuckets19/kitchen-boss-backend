package com.easyBucketSoft19.NGR.entity.order;

import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.utils.constant.OrderStatus;
import com.easyBucketSoft19.NGR.utils.constant.OrderType;
import com.easyBucketSoft19.NGR.utils.listener.PriceListener;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "orders")
@Getter @Setter
@EntityListeners(PriceListener.class)
public class Order {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Integer id;
    /* ------------------------------------------------------------------------------------ */

    @Column(nullable = false)
    private BigDecimal total;

    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;

    @Enumerated(EnumType.STRING)
    private OrderType orderType;

    /* ------------------------------- ENTITY RELATIONSHIPS -------------------------------  */
    @OneToMany( fetch = FetchType.EAGER,
        cascade = CascadeType.ALL,
        mappedBy = "order",
        orphanRemoval = true)
    private List<OrderLine> orderLines = new ArrayList<>();

    @ManyToOne @JoinColumn(name = "customer_id")
    private User customer;

    @ManyToOne @JoinColumn(name = "delivery_id")
    private User delivery;

    @ManyToOne @JoinColumn(name = "restaurant_id")
    private Restaurant restaurant;
    /* ------------------------------------------------------------------------------------ */

    public Order() {}

    public void addLineItem(OrderLine line) {
        orderLines.add(line);
        line.setOrder(this);
    }

    public void removeLineItem(OrderLine line) {
        orderLines.remove(line);
        line.setOrder(null);
    }

    /* --------------------------------- ENTITY LIFECYCLE ---------------------------------  */
    @PrePersist                 private void onPersist() {
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }
    @PreUpdate                  private void onUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
    @Setter(AccessLevel.NONE)   private LocalDateTime createdAt;
    @Setter(AccessLevel.NONE)   private LocalDateTime updatedAt;
    /* ------------------------------------------------------------------------------------  */


}