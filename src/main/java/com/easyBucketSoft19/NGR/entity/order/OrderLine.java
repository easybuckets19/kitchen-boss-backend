package com.easyBucketSoft19.NGR.entity.order;

import com.easyBucketSoft19.NGR.entity.food.Food;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity(name = "order_lines")
@Getter @Setter
public class OrderLine {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Integer id;
    /* ------------------------------------------------------------------------------------ */

    @Column(nullable = false)
    private BigDecimal currentPrice;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = false)
    private BigDecimal lineTotal;

    /* ------------------------------- ENTITY RELATIONSHIPS -------------------------------  */
    @ManyToOne(fetch = FetchType.LAZY)
    private Food food;

    @ManyToOne(fetch = FetchType.LAZY)
    private Order order;
    /* ------------------------------------------------------------------------------------ */

    public OrderLine() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        OrderLine orderLine = (OrderLine) o;

        return id.equals(orderLine.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
