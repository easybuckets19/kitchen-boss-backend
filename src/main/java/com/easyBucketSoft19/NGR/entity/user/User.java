package com.easyBucketSoft19.NGR.entity.user;

import com.easyBucketSoft19.NGR.entity.restaurant.Place;
import com.easyBucketSoft19.NGR.utils.constant.UserStatus;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "users")
@Getter @Setter
public class User {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Integer id;
    /* ------------------------------------------------------------------------------------ */

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false, unique = true)
    private String email;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    private String firstName;

    private String lastName;

    /* ------------------------------- ENTITY RELATIONSHIPS -------------------------------  */
    @OneToOne(fetch = FetchType.EAGER, cascade = { CascadeType.ALL })
    @JoinColumn(name = "place_id", referencedColumnName = "id")
    private Place address;

    @ManyToMany(fetch = FetchType.EAGER, cascade = { CascadeType.MERGE, CascadeType.PERSIST })
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles = new HashSet<>();
    /* ------------------------------------------------------------------------------------ */

    public User() {}

    public User(String username, String password, String email, String firstName, String lastName) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /* --------------------------------- ENTITY LIFECYCLE ---------------------------------  */
    @PrePersist                 private void onPersist() {
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }
    @PreUpdate                  private void onUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
    @Setter(AccessLevel.NONE)   private LocalDateTime createdAt;
    @Setter(AccessLevel.NONE)   private LocalDateTime updatedAt;
    /* ------------------------------------------------------------------------------------  */

}
