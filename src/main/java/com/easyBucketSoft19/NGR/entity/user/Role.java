package com.easyBucketSoft19.NGR.entity.user;

import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "roles")
@Getter @Setter
public class Role {
    /* -------------------------------- ENTITY PRIMARY KEY --------------------------------  */
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Setter(AccessLevel.NONE)
    private Short id;
    /* ------------------------------------------------------------------------------------ */

    @Column(unique = true) @Enumerated(EnumType.STRING)
    private UserRole role;

    public Role() {}

    public Role(UserRole role) {
        this.role = role;
    }

    /* --------------------------------- ENTITY LIFECYCLE ---------------------------------  */
    @PrePersist                 private void onPersist() {
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }
    @PreUpdate                  private void onUpdate() {
        this.updatedAt = LocalDateTime.now();
    }
    @Setter(AccessLevel.NONE)   private LocalDateTime createdAt;
    @Setter(AccessLevel.NONE)   private LocalDateTime updatedAt;
    /* ------------------------------------------------------------------------------------  */

}
