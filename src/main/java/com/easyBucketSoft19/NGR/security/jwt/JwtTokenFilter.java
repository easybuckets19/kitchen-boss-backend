package com.easyBucketSoft19.NGR.security.jwt;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@Slf4j
public class JwtTokenFilter extends OncePerRequestFilter {
    private final UserDetailsService  userDetailsService;
    private final JwtTokenService     jwtTokenService;

    public JwtTokenFilter(UserDetailsService userDetailsService, JwtTokenService jwtTokenService) {
        this.userDetailsService = userDetailsService;
        this.jwtTokenService = jwtTokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        try {
            // Extract the token from the using the extractFromHeader() method.
            String jwtToken = extractToken(request);
            if (jwtToken != null && jwtTokenService.validateJWT(jwtToken)) {
                // Extract the username from the JWT to perform DB lookup.
                String username = jwtTokenService.extractSubject(jwtToken);
                // Searching the User in the DB based on the previous username.
                var details = userDetailsService.loadUserByUsername(username);
                // Generating an internal token for the Security Framework.
                var internalToken = new UsernamePasswordAuthenticationToken(details, null, details.getAuthorities());
                // Setting the SecurityContext with the specified token.
                SecurityContextHolder.getContext().setAuthentication(internalToken);
            }
        } catch (UsernameNotFoundException ex) {
            log.warn("Az adott felhasználó nem létezik az adatbázisban. [exception: {}]", ex.getMessage());
            SecurityContextHolder.clearContext();
        }
        filterChain.doFilter(request, response);
    }

    private String extractToken(HttpServletRequest request){
        // Accessing the request AUTHORIZATION header specified in the config file.
        String authHeader = request.getHeader(jwtTokenService.getJwtHeader());
        // Accessing the request AUTHORIZATION param specified in the config file.
        String authParam  = request.getParameter("access_token");
        // Checking if the token starts with BEARER prefix specified in the config. [+extra empty check]
        if (StringUtils.hasText(authHeader) && authHeader.startsWith(jwtTokenService.getJwtPrefix())){
            return authHeader.split(" ")[1].trim();
        }
        else if (StringUtils.hasText(authParam)) {
            return authParam;
        }
        return null;
    }

}
