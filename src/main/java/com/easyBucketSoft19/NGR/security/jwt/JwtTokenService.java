package com.easyBucketSoft19.NGR.security.jwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j @Service
public class JwtTokenService {
    @Value("${NGR.jwt.header}")          private String  jwtHeader;
    @Value("${NGR.jwt.prefix}")          private String  jwtPrefix;
    @Value("${NGR.jwt.expiration}")      private long    jwtExpirationMs;
    private final SecretKey key = Keys.secretKeyFor(SignatureAlgorithm.HS512);

    public String generateJWT(UserDetails userDetails) {
        // Accessing the builder() INSTANCE to generate the token.
        JwtBuilder builder = Jwts.builder();
        // Setting the specific parts of the JWT [SUBJECT, ISSUED_AT, EXPIRATION, CLAIMS]
        builder
            .setSubject(userDetails.getUsername())
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + jwtExpirationMs))
            .claim("role", buildAuthorityClaim(userDetails.getAuthorities()));
        // Sign the JWT with the IN-MEMORY generated secure key.
        return builder.signWith(key).compact();
    }

    public String extractSubject(String token) {
        // Accessing the builder() INSTANCE to parse the token.
        JwtParser parser = Jwts.parserBuilder().setSigningKey(key).build();
        // Parse claims to extract the SUBJECT part or the token.
        return parser.parseClaimsJws(token).getBody().getSubject();
    }

    public Boolean validateJWT(String token) {
        // Accessing the builder() INSTANCE to parse the token.
        JwtParser parser = Jwts.parserBuilder().setSigningKey(key).build();
        try {
            parser.parseClaimsJws(token); return true;
        } catch (ExpiredJwtException e) {
            log.warn("The given JWT is expired. [exception: {}]", e.getMessage());
        } catch (MalformedJwtException e) {
            log.warn("The given JWT is malformed. [exception: {}]", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.warn("The given JWT is not supported. [exception: {}]", e.getMessage());
        } catch (SecurityException e) {
            log.warn("The given JWT is not secured. [exception: {}]", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.warn("The given JWT's claims are invalid. [exception: {}]", e.getMessage());
        }
        return false;
    }

    public String getJwtHeader() {
        return jwtHeader;
    }

    public String getJwtPrefix() {
        return jwtPrefix;
    }

    public String getJwtSecretString(){
        return Encoders.BASE64.encode(key.getEncoded());
    }

    private List<String> buildAuthorityClaim(Collection<? extends GrantedAuthority> authorities){
        return authorities.stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList());
    }

}

