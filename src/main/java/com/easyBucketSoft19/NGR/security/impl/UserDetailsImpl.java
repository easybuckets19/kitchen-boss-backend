package com.easyBucketSoft19.NGR.security.impl;

import com.easyBucketSoft19.NGR.entity.user.Role;
import com.easyBucketSoft19.NGR.utils.constant.UserStatus;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data @Builder
public class UserDetailsImpl implements UserDetails {
    private static final long serialVersionUID = 1L;

    private final Integer id;
    private final String username;
    private final String password;
    private final Set<Role> roles;
    private final UserStatus status;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Stream<Role> roleStream = this.roles.stream();
        return roleStream
            .map(role -> role.getRole().name())
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return status != UserStatus.STATUS_DISABLED;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return status == UserStatus.STATUS_ACCEPTED;
    }
}