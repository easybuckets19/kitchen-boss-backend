package com.easyBucketSoft19.NGR.security.impl;


import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.findByUsername(username)
            .orElseThrow(() -> new UsernameNotFoundException(String.format
                    ("User was not found with given {username=%s}.", username)
                )
            );
        return buildUserDetails(user);
    }

    private UserDetails buildUserDetails(User user) {
        return UserDetailsImpl.builder()
            .id(user.getId())
            .username(user.getUsername())
            .password(user.getPassword())
            .status(user.getStatus())
            .roles(user.getRoles())
            .build();
    }
}
