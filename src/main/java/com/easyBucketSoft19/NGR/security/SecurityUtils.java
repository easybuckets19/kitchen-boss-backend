package com.easyBucketSoft19.NGR.security;

import com.easyBucketSoft19.NGR.security.impl.UserDetailsImpl;
import lombok.experimental.UtilityClass;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

@UtilityClass
public final class SecurityUtils {

    public UserDetailsImpl getAuthenticatedUser(){
        return (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
