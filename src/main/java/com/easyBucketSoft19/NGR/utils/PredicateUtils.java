package com.easyBucketSoft19.NGR.utils;

import com.easyBucketSoft19.NGR.api.GoogleApi;
import com.easyBucketSoft19.NGR.data.output.PlaceDistanceDTO;
import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

@UtilityClass
public class PredicateUtils {

    public Predicate<User> rolePredicate(UserRole userRole){
        return user -> user.getRoles().iterator().next().getRole().equals(userRole);
    }

    public BiPredicate<User, Restaurant> distancePredicate(Long distance, GoogleApi api){
        return (user, restaurant) -> {
            try {
                String originId = restaurant.getAddress().getId();
                String destId   = user.getAddress().getId();
                PlaceDistanceDTO distanceDTO = api.placeDistance(originId, destId);
                System.out.println(String.format("- RESTAURANT: %s \t DELIVERY: %s \tDISTANCE: %s",
                    restaurant.getAddress().getAddress(),
                    user.getAddress().getAddress(),
                    distanceDTO.getDistance()));
                return distanceDTO.getDistance() < distance;
            } catch (IOException exception) {
                return false;
            }
        };
    }
}
