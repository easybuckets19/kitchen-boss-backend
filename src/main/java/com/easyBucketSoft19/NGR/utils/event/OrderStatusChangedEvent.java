package com.easyBucketSoft19.NGR.utils.event;

import com.easyBucketSoft19.NGR.entity.order.Order;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter @Setter
public class OrderStatusChangedEvent extends ApplicationEvent {
    private final Order order;

    public OrderStatusChangedEvent(Object source, Order order) {
        super(source);
        this.order = order;
    }
}
