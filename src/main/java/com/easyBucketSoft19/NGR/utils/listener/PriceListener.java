package com.easyBucketSoft19.NGR.utils.listener;

import com.easyBucketSoft19.NGR.entity.order.Order;
import com.easyBucketSoft19.NGR.entity.order.OrderLine;
import com.sun.el.stream.Stream;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import java.math.BigDecimal;
import java.util.List;

@Component
public class PriceListener {

    @PrePersist
    void onPersist(Order order) {
        List<OrderLine> orderLines = order.getOrderLines();
        orderLines.forEach(it -> {
            it.setCurrentPrice(it.getFood().getPrice());
            it.setLineTotal(calcLine(it));
        });
        order.setTotal(calcTotal(order.getOrderLines()));
    }

    private BigDecimal calcLine(OrderLine lineItem) {
        Integer quantity = lineItem.getQuantity();
        return lineItem.getCurrentPrice().multiply(BigDecimal.valueOf(quantity));
    }

    private BigDecimal calcTotal(List<OrderLine> orderLines) {
        return orderLines.stream()
            .map(OrderLine::getLineTotal)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
