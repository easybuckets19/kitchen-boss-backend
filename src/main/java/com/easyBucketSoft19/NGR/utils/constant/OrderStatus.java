package com.easyBucketSoft19.NGR.utils.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor @Getter
public enum OrderStatus {
    STATUS_PENDING("pending"),
    STATUS_ACCEPTED("accepted"),
    STATUS_REJECTED("rejected"),
    STATUS_DONE("done");
    private final String value;
}
