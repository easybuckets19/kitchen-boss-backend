package com.easyBucketSoft19.NGR.utils.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor @Getter
public enum OrderType {
    TYPE_DELIVERY("delivery"),
    TYPE_RESTAURANT("restaurant");
    private final String value;
}

