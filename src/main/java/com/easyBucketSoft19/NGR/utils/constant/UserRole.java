package com.easyBucketSoft19.NGR.utils.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor @Getter
public enum UserRole {
    ROLE_ADMIN("admin"),
    ROLE_CUSTOMER("customer"),
    ROLE_DELIVERY("delivery"),
    ROLE_OWNER("owner");
    private final String value;
}