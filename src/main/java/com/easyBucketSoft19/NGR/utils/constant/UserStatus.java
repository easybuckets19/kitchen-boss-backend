package com.easyBucketSoft19.NGR.utils.constant;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor @Getter
public enum UserStatus {
    STATUS_ACCEPTED("accepted"),
    STATUS_PENDING("pending"),
    STATUS_DISABLED("disabled");
    private final String value;
}
