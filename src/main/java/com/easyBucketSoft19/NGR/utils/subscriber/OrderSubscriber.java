package com.easyBucketSoft19.NGR.utils.subscriber;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import java.io.IOException;

public class OrderSubscriber extends SseEmitter {
    private final UserDetails principal;

    public OrderSubscriber(UserDetails principal) {
        super(-1L);
        this.principal = principal;
        init();
    }

    private void init() {
        try {
            SseEventBuilder initEvent = SseEmitter.event().data("").name("listen_init");
            this.send(initEvent);
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public UserDetails getPrincipal() {
        return principal;
    }
}
