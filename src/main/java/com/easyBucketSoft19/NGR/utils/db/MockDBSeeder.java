package com.easyBucketSoft19.NGR.utils.db;

import com.easyBucketSoft19.NGR.api.GoogleApi;
import com.easyBucketSoft19.NGR.data.output.PlaceDetailsDTO;
import com.easyBucketSoft19.NGR.entity.food.Allergen;
import com.easyBucketSoft19.NGR.entity.food.Category;
import com.easyBucketSoft19.NGR.entity.restaurant.Place;
import com.easyBucketSoft19.NGR.entity.user.Role;
import com.easyBucketSoft19.NGR.entity.user.User;
import com.easyBucketSoft19.NGR.repository.food.AllergenRepository;
import com.easyBucketSoft19.NGR.repository.food.CategoryRepository;
import com.easyBucketSoft19.NGR.repository.user.RoleRepository;
import com.easyBucketSoft19.NGR.repository.user.UserRepository;
import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import com.easyBucketSoft19.NGR.utils.constant.UserStatus;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

@Slf4j
@Component @Transactional
public class MockDBSeeder implements CommandLineRunner {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final CategoryRepository categoryRepository;
    private final AllergenRepository allergenRepository;
    private final GoogleApi api;
    private final PasswordEncoder encoder;

    @Autowired
    public MockDBSeeder(UserRepository userRepository, RoleRepository roleRepository, CategoryRepository categoryRepository, AllergenRepository allergenRepository, GoogleApi api, PasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.categoryRepository = categoryRepository;
        this.allergenRepository = allergenRepository;
        this.api = api;
        this.encoder = encoder;
    }

    @Override
    public void run(String... args) {
        /* --------------- SEEDING 1.0 -------------- */
        log.info("Seeding ROLES started. [elapsed: {} ms]", 0);
        long start1 = System.currentTimeMillis();
        List<Role> persistedRoles = seedRoles();
        log.info("Seeding ROLES finished. [elapsed: {} ms]", ( System.currentTimeMillis()-start1));
        /* ------------------------------------------ */


        /* --------------- SEEDING 2.0 -------------- */
        log.info("Seeding USERS started. [elapsed: {} ms]", 0);
        long start2 = System.currentTimeMillis();
        List<User> persistedUsers = seedUsers(persistedRoles);
        log.info("Seeding USERS finished. [elapsed: {} ms]", ( System.currentTimeMillis()-start2));
        /* ------------------------------------------ */

        /* --------------- SEEDING 3.0 -------------- */
        log.info("Seeding CATEGORIES started. [elapsed: {} ms]", 0);
        long start3 = System.currentTimeMillis();
        List<Category> persistedCategories = seedCategories();
        log.info("Seeding CATEGORIES finished. [elapsed: {} ms]", ( System.currentTimeMillis()-start2));
        /* ------------------------------------------ */

        /* --------------- SEEDING 4.0 -------------- */
        log.info("Seeding CATEGORIES started. [elapsed: {} ms]", 0);
        long start4 = System.currentTimeMillis();
        List<Allergen> persistedAllergens = seedAllergens();
        log.info("Seeding CATEGORIES finished. [elapsed: {} ms]", ( System.currentTimeMillis()-start2));
        /* ------------------------------------------ */

    }

    @SneakyThrows
    private List<User> seedUsers(List<Role> roles) {
        User admin = new User("admin123", encoder.encode("admin123"), "admin1@gmail.com", "Admin1", "Admin1");
        User customer = new User("customer123", encoder.encode("customer123"), "customer1@gmail.com", "Customer1", "Customer1");
        User delivery = new User("delivery123", encoder.encode("delivery123"), "delivery1@gmail.com", "Delivery1", "Delivery1");
        User owner = new User("owner123", encoder.encode("owner123"), "owner1@gmail.com", "Owner1", "Owner1");

        User admin2 = new User("admin1234", encoder.encode("admin1234"), "admin2@gmail.com", "Admin2", "Admin2");
        User customer2 = new User("customer1234", encoder.encode("customer1234"), "customer2@gmail.com", "Customer2", "Customer2");
        User delivery2 = new User("delivery1234", encoder.encode("delivery1234"), "delivery2@gmail.com", "Delivery2", "Delivery2");
        User owner2 = new User("owner1234", encoder.encode("owner1234"), "owner2@gmail.com", "Owner", "Owner2");

        PlaceDetailsDTO place1 = api.placeDetails("ChIJgV4BZRW9aUcRg8PZ20zjT_U");
        PlaceDetailsDTO place2 = api.placeDetails("ChIJ514tRRO9aUcRm78xQtLvm2Q");
        PlaceDetailsDTO place3 = api.placeDetails("ChIJW8SBZBW9aUcRqKp30OdJppc");
        PlaceDetailsDTO place4 = api.placeDetails("ChIJJbxCPBS9aUcRCWSxzOGGVP4");

        customer.setAddress(new Place(place1.getId(), place1.getAddress(), place1.getLat(), place1.getLng(), null));
        delivery.setAddress(new Place(place2.getId(), place2.getAddress(), place2.getLat(), place2.getLng(), 2000L));
        customer2.setAddress(new Place(place3.getId(), place3.getAddress(), place3.getLat(), place3.getLng(), null));
        delivery2.setAddress(new Place(place4.getId(), place4.getAddress(), place4.getLat(), place4.getLng(), 1500L));

        admin.setStatus(UserStatus.STATUS_ACCEPTED);    admin2.setStatus(UserStatus.STATUS_ACCEPTED);
        customer.setStatus(UserStatus.STATUS_ACCEPTED); customer2.setStatus(UserStatus.STATUS_ACCEPTED);
        delivery.setStatus(UserStatus.STATUS_ACCEPTED); delivery2.setStatus(UserStatus.STATUS_ACCEPTED);
        owner.setStatus(UserStatus.STATUS_ACCEPTED);    owner2.setStatus(UserStatus.STATUS_ACCEPTED);

        admin.setRoles(Set.of(roles.get(0)));           admin2.setRoles(Set.of(roles.get(0)));
        customer.setRoles(Set.of(roles.get(1)));        customer2.setRoles(Set.of(roles.get(1)));
        delivery.setRoles(Set.of(roles.get(2)));        delivery2.setRoles(Set.of(roles.get(2)));
        owner.setRoles(Set.of(roles.get(3)));           owner2.setRoles(Set.of(roles.get(3)));

        return userRepository.saveAll(List.of(admin, customer, delivery, owner, admin2, customer2, owner2, delivery2));
    }

    private List<Role> seedRoles() {
        Role adminRole = new Role(UserRole.ROLE_ADMIN);
        Role customerRole = new Role(UserRole.ROLE_CUSTOMER);
        Role deliveryRole = new Role(UserRole.ROLE_DELIVERY);
        Role ownerRole = new Role(UserRole.ROLE_OWNER);
        return roleRepository.saveAll(List.of(adminRole, customerRole, deliveryRole, ownerRole));
    }

    private List<Category> seedCategories() {
        Category mainDish = new Category("Főételek");
        Category soupDish = new Category("Levesek");
        return categoryRepository.saveAll(List.of(mainDish, soupDish));
    }

    private List<Allergen> seedAllergens() {
        Allergen soya = new Allergen("Szója");
        Allergen nut = new Allergen("Dió");
        return allergenRepository.saveAll(List.of(soya, nut));
    }
}
