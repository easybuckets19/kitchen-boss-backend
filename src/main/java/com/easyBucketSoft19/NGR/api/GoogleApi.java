package com.easyBucketSoft19.NGR.api;

import com.easyBucketSoft19.NGR.data.output.PlaceDetailsDTO;
import com.easyBucketSoft19.NGR.data.output.PlaceDistanceDTO;
import com.easyBucketSoft19.NGR.data.output.PlaceLookupDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;

@Component
public class GoogleApi {
    @Value("${NGR.api.google.host}")    String   apiHost;
    @Value("${NGR.api.google.key}")     String   apiKey;
    @Value("${NGR.api.google.paths}")   String[] apiPaths;

    private final ApiHandler apiHandler;
    private final ObjectMapper objectMapper;

    @Autowired
    public GoogleApi(ApiHandler apiHandler, ObjectMapper objectMapper) {
        this.apiHandler = apiHandler;
        this.objectMapper = objectMapper;
    }

    public PlaceDetailsDTO placeDetails(String placeId) throws IOException {
        URI apiUrl = buildURI(apiPaths[0], Map.of(
        "place_id", placeId)
        );
        ResponseEntity<String> responseEntity = this.apiHandler.executeCall(apiUrl, HttpMethod.GET, HttpHeaders.EMPTY, null, String.class);
        JsonNode node       = objectMapper.readTree(responseEntity.getBody()).get("result");
        ObjectReader reader = objectMapper.readerFor(new TypeReference<PlaceDetailsDTO>() {});
        return reader.readValue(node);
    }

    public List<PlaceLookupDTO> placeAutoComplete(String placeName) throws IOException {
        URI apiUrl = buildURI(apiPaths[1], Map.of(
            "input", placeName,
            "type", "address",
            "components", "country:hu")
        );
        ResponseEntity<String> responseEntity = this.apiHandler.executeCall(apiUrl, HttpMethod.GET, HttpHeaders.EMPTY, null, String.class);
        JsonNode node       = objectMapper.readTree(responseEntity.getBody()).get("predictions");
        ObjectReader reader = objectMapper.readerFor(new TypeReference<List<PlaceLookupDTO>>() {});
        return reader.readValue(node);
    }

    public PlaceDistanceDTO placeDistance(String originId, String destId) throws IOException {
        URI apiUrl = buildURI(apiPaths[2], Map.of(
        "origins", "place_id:".concat(originId),
        "destinations", "place_id:".concat(destId))
        );
        ResponseEntity<String> responseEntity = this.apiHandler.executeCall(apiUrl, HttpMethod.GET, HttpHeaders.EMPTY, null, String.class);
        JsonNode node       = objectMapper.readTree(responseEntity.getBody());
        ObjectReader reader = objectMapper.readerFor(new TypeReference<PlaceDistanceDTO>() {});
        return reader.readValue(node);
    }

    URI buildURI(String apiPath, Map<String, String> queryParams) {
        // Accessing the builder() INSTANCE to generate the token.
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance();
        // Setting the static parts of the URI [SCHEME, HOST, PATH]
        builder = builder.scheme("https").host(apiHost)
            .path(apiPath.concat("/json"))
            .queryParam("key", apiKey);
        // Using a lambda reference to add the dynamic params.
        queryParams.forEach(builder::queryParam);
        // Build the custom URI used to exchange a REST call.
        return builder.build().toUri();
    }

}

