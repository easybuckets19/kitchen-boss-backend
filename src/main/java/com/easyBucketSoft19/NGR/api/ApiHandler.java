package com.easyBucketSoft19.NGR.api;

import com.sun.istack.Nullable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Slf4j @Component
public class ApiHandler {
    private final RestTemplate restTemplate;

    public ApiHandler() {
        this.restTemplate = new RestTemplate();
    }

    public <T, R> ResponseEntity<R> executeCall(URI apiUrl, HttpMethod method, HttpHeaders headers, @Nullable T requestObj, Class<R> responseObj){
        HttpEntity<T> httpEntity = new HttpEntity<>(requestObj, headers);
        try {
            return this.restTemplate.exchange(apiUrl, method, httpEntity, responseObj);
        } catch (RestClientException ex){
            log.error("External API call failed with exception [message: {}]", ex.getMessage());
            throw new RuntimeException(ex);
        }
    }
}

