package com.easyBucketSoft19.NGR.repository.user;

import com.easyBucketSoft19.NGR.entity.user.Role;
import com.easyBucketSoft19.NGR.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    /**
     * Checks the {@link User}'s existence in the database by the given <b>username</b>.
     *
     * @param username  The WHERE condition in the generated query.
     * @return          A simple {@link Boolean} value based on the existence.
     */
    Boolean existsByUsername(String username);

    /**
     * Checks the {@link User}'s existence in the database by the given <b>email</b>.
     *
     * @param email     The WHERE condition in the generated query.
     * @return          A simple {@link Boolean} value based on the existence.
     */
    Boolean existsByEmail(String email);

    /**
     * Returns the {@link User} from the database by the given <b>email</b>.
     *
     * @param email     The WHERE condition in the generated query.
     * @return          An {@link Optional} representation of the {@link User}, which must be checked.
     */
    Optional<User> findByEmail(String email);

    /**
     * Returns the {@link User} from the database by the given <b>email</b>.
     *
     * @param username  The WHERE condition in the generated query.
     * @return          An {@link Optional} representation of the {@link User}, which must be checked.
     */
    Optional<User> findByUsername(String username);

}
