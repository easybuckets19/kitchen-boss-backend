package com.easyBucketSoft19.NGR.repository.user;


import com.easyBucketSoft19.NGR.entity.user.Role;
import com.easyBucketSoft19.NGR.utils.constant.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Short> {
    /**
     * Checks the {@link Role}'s existence in the database by the given {@link UserRole} <b>constant</b>.
     *
     * @param role      The WHERE condition in the generated query.
     * @return          A simple {@link Boolean} value based on the existence.
     */
    Boolean existsByRole(UserRole role);

    /**
     * Returns the {@link Role} from the database by the given {@link UserRole} <b>constant</b>.
     *
     * @param role      The WHERE condition in the generated query.
     * @return          An {@link Optional} representation of the {@link Role}, which must be checked.
     */
    Optional<Role> findByRole(UserRole role);
}
