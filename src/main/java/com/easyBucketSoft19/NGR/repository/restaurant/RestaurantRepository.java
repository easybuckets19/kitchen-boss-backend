package com.easyBucketSoft19.NGR.repository.restaurant;

import com.easyBucketSoft19.NGR.entity.restaurant.Restaurant;
import com.easyBucketSoft19.NGR.entity.restaurant.Style;
import com.easyBucketSoft19.NGR.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RestaurantRepository extends JpaRepository<Restaurant, Integer> {

    Boolean existsByName(String name);

    Optional<Restaurant> findByName(String name);

    Optional<Restaurant> findByOwner(User owner);

    List<Restaurant> findByOwnerUsername(String username);

}
