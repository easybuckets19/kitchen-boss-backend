package com.easyBucketSoft19.NGR.repository.restaurant;


import com.easyBucketSoft19.NGR.entity.restaurant.Style;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StyleRepository extends JpaRepository<Style, Short> {
    /**
     * Checks the {@link Style}'s existence in the database by the given {@link String} <b>constant</b>.
     *
     * @param style      The WHERE condition in the generated query.
     * @return          A simple {@link Boolean} value based on the existence.
     */
    Boolean existsByStyle(String style);

    /**
     * Returns the {@link Style} from the database by the given {@link String} <b>constant</b>.
     *
     * @param style     The WHERE condition in the generated query.
     * @return          An {@link Optional} representation of the {@link Style}, which must be checked.
     */
    Optional<Style> findByStyle(String style);
}
