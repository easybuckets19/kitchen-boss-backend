package com.easyBucketSoft19.NGR.repository.restaurant;

import com.easyBucketSoft19.NGR.entity.restaurant.Place;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaceRepository extends JpaRepository<Place, String> {
}
