package com.easyBucketSoft19.NGR.repository.food;

import com.easyBucketSoft19.NGR.entity.food.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Short> {
    /**
     * Checks the {@link Category}'s existence in the database by the given {@link String} <b>constant</b>.
     *
     * @param category  The WHERE condition in the generated query.
     * @return          A simple {@link Boolean} value based on the existence.
     */
    Boolean existsByCategory(String category);

    /**
     * Returns the {@link Category} from the database by the given {@link String} <b>constant</b>.
     *
     * @param category  The WHERE condition in the generated query.
     * @return          An {@link Optional} representation of the {@link Category}, which must be checked.
     */
    Optional<Category> findByCategory(String category);
}
