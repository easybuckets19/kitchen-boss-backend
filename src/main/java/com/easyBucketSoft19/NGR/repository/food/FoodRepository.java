package com.easyBucketSoft19.NGR.repository.food;

import com.easyBucketSoft19.NGR.entity.food.Food;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FoodRepository extends JpaRepository<Food, Integer> {

    @Query(value = "select f from foods f where f.restaurant.id =?1")
    List<Food> findByRestaurant(Integer restaurantId);

}
