package com.easyBucketSoft19.NGR.repository.food;

import com.easyBucketSoft19.NGR.entity.food.Allergen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AllergenRepository extends JpaRepository<Allergen, Short> {
    /**
     * Checks the {@link Allergen}'s existence in the database by the given {@link String} <b>constant</b>.
     *
     * @param allergen  The WHERE condition in the generated query.
     * @return          A simple {@link Boolean} value based on the existence.
     */
    Boolean existsByAllergen(String allergen);

    /**
     * Returns the {@link Allergen} from the database by the given {@link String} <b>constant</b>.
     *
     * @param allergen  The WHERE condition in the generated query.
     * @return          An {@link Optional} representation of the {@link Allergen}, which must be checked.
     */
    Optional<Allergen> findByAllergen(String allergen);
}
