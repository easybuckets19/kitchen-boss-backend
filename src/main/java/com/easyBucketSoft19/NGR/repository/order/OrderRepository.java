package com.easyBucketSoft19.NGR.repository.order;

import com.easyBucketSoft19.NGR.entity.order.Order;
import com.easyBucketSoft19.NGR.utils.constant.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Set;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    @Query(value = "select o from orders o where o.delivery.id =?1 and o.orderStatus IN ?2")
    List<Order> findByDelivery(Integer userId, Set<OrderStatus> status);

    @Query(value = "select o from orders o where o.restaurant.id =?1 and o.orderStatus IN ?2")
    List<Order> findByRestaurant(Integer restaurantId, Set<OrderStatus> status);

}
